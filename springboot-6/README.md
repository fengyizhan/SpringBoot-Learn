#SpringBoot初始教程之数据库连接池(druid)(六)
##1.介绍
Druid是一个JDBC组件库，包括数据库连接池、SQL Parser等组件。DruidDataSource是最好的数据库连接池。SpringBoot支持任何一种数据库链接池的配置，在这里用druid作为例子进行讲解
##2.快速开始
这块先以Spring的JdbcTemplate为列子进行讲解
###pom.xml
```xml

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <parent>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>1.4.1.RELEASE</version>
        </parent>
        <modelVersion>4.0.0</modelVersion>
    
        <artifactId>springboot-6</artifactId>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-jdbc</artifactId>
            </dependency>
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>5.1.39</version>
            </dependency>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>1.0.26</version>
            </dependency>
        </dependencies>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>1.4.1.RELEASE</version>
                    <configuration>
                        <fork>true</fork>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    
    </project>

```

配置统一的DataSource,这种方式不太合适生产环境，SpringBoot可以统一的配置application.yaml，但是目前仅仅支持dbcp、dbcp2、hikari
下面这种方式无法不支持的DruidDataSource的其他参数

###application.yaml

```yaml

    spring:
      datasource:
        driver-class-name: com.mysql.jdbc.Driver
        username: root
        password: qq123456
        url: jdbc:mysql://localhost:3306/test1
        type: com.alibaba.druid.pool.DruidDataSource

```

###通过代码的方式进行配置

```java

    @SpringBootApplication
    public class AppApplication {
    
        public static void main(String[] args) throws Exception {
            SpringApplication.run(AppApplication.class, args);
        }
    
        /**
         * 注册DruidServlet
         *
         * @return
         */
        @Bean
        public ServletRegistrationBean druidServletRegistrationBean() {
            ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
            servletRegistrationBean.setServlet(new StatViewServlet());
            servletRegistrationBean.addUrlMappings("/druid/*");
            return servletRegistrationBean;
        }
    
        /**
         * 注册DruidFilter拦截
         *
         * @return
         */
        @Bean
        public FilterRegistrationBean duridFilterRegistrationBean() {
            FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
            filterRegistrationBean.setFilter(new WebStatFilter());
            Map<String, String> initParams = new HashMap<String, String>();
            //设置忽略请求
            initParams.put("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
            filterRegistrationBean.setInitParameters(initParams);
            filterRegistrationBean.addUrlPatterns("/*");
            return filterRegistrationBean;
        }
    
        /**
         * 配置DataSource
         * @return
         * @throws SQLException
         */
        @Bean
        public DataSource druidDataSource() throws SQLException {
            DruidDataSource druidDataSource = new DruidDataSource();
            druidDataSource.setUsername("root");
            druidDataSource.setPassword("qq123456");
            druidDataSource.setUrl("jdbc:mysql://localhost:3306/test1");
            druidDataSource.setMaxActive(100);
            druidDataSource.setFilters("stat,wall");
            druidDataSource.setInitialSize(10);
            return druidDataSource;
        }
    
    }


```

这块采用的是`ServletRegistrationBean` 和`FilterRegistrationBean`的方式进行注册Servlet和Filter，这种是SpingBoot里面提供支持原生的方式
除了这种方式还可以采用其他方式进行配置，采用Servlet3.0的注解Servlet进行配置

这块配置基本就完事了，可以访问本地链接http://localhost:8080/druid/datasource.html查看监控信息

##官方资料
其他的详细配置可以查看官方文档进行配置，这里不过多讲述
https://github.com/alibaba/druid/wiki/%E9%A6%96%E9%A1%B5

###本文代码

https://git.oschina.net/wangkang_daydayup/SpringBoot-Learn/tree/master

springboot-6