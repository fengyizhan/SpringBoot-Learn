#SpringBoot初始教程之Redis集中式Session管理(四)

##1.介绍
有关Session的管理方式这里就不再进行讨论，目前无非就是三种单机Session(基于单机内存，无法部署多台机器)、基于Cookie(安全性差)、基于全局的统一Session管理(redis、mysql)等多种方式
针对于像淘宝这种超大型网站来说Session如何管理的就无从得知了、但是可以通过yy的方式想象一下，这种大型架构都需要部署多台认证Server，但是一般来说集中式Session无法存储那么多的Session
那么就可以通过UID分片的形式来存储，不同UID分布在不同的Server上认证即可(纯属猜测丷)

##2.快速开始

这里采用的是redis进行集中式Session管理,核心如下依赖

```xml
            <dependency>
                <groupId>org.springframework.session</groupId>
                <artifactId>spring-session</artifactId>
                <version>1.2.2.RELEASE</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-redis</artifactId>
            </dependency>
```
###完整pom.xml

```xml

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <parent>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>1.4.1.RELEASE</version>
        </parent>
        <modelVersion>4.0.0</modelVersion>
    
        <artifactId>springboot-4</artifactId>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-freemarker</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.session</groupId>
                <artifactId>spring-session</artifactId>
                <version>1.2.2.RELEASE</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-redis</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <scope>test</scope>
            </dependency>
        </dependencies>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>1.4.1.RELEASE</version>
                </plugin>
            </plugins>
        </build>
    
    </project>

```

### application.yaml配置
这块主要是通过application.yaml去配置redis的链接信息
```yaml

    server:
      port: 8080
    spring:
      redis:
        database: 1
        host: localhost
        pool:
          max-active: 20

```

### 开启`@EnableRedisHttpSession`

通过加上`@EnableRedisHttpSession`注解，开启redis集中式session管理，所有的session都存放到了redis中

```java

    @SpringBootApplication
    @EnableRedisHttpSession
    public class AppApplication {
        public static void main(String[] args) throws Exception {
            SpringApplication.run(AppApplication.class, args);
        }
    }

```
通过源码可知、可以通过设置maxInactiveIntervalInSeconds来设定session的统一过期时间，

```java

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @Target({ java.lang.annotation.ElementType.TYPE })
    @Documented
    @Import(RedisHttpSessionConfiguration.class)
    @Configuration
    public @interface EnableRedisHttpSession {
        int maxInactiveIntervalInSeconds() default 1800;
    
        String redisNamespace() default "";
    
        
        RedisFlushMode redisFlushMode() default RedisFlushMode.ON_SAVE;
    }

```

通过redis集中式管理session这种方式在使用上面对客户端是透明的，无需自己操作redis，在使用HttpSession对象的时候直接使用即可

```java

    @RestController
    public class IndexController {
        @GetMapping("/index")
        public ResponseEntity index(HttpSession httpSession) {
            httpSession.setAttribute("user", "helloword");
            return ResponseEntity.ok("ok");
        }
    
        @GetMapping("/helloword")
        public ResponseEntity hello(HttpSession httpSession) {
            return ResponseEntity.ok(httpSession.getAttribute("user"));
        }
    }

```


##3. 其他扩展
SpringBoot Session集中式管理不仅仅限于redis这种方式，目前内部支持HttpSession with Pivotal GemFire、HttpSession with JDBC、HttpSession with Mongo
等多种方式具体可以参考http://docs.spring.io/spring-session/docs/1.2.2.RELEASE/reference/html5/#httpsession-jdbc
本文代码

https://git.oschina.net/wangkang_daydayup/SpringBoot-Learn/tree/master

springboot-4