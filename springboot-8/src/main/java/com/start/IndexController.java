package com.start;

import com.start.entity.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: IndexController
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/8
 */
@RestController
public class IndexController {
    @GetMapping("/index")
    public ResponseEntity jdbcTemplateDBCP2() {
        return ResponseEntity.ok("ok");
    }

    @GetMapping("/hello")
    public ResponseEntity indexEntity() {
        Person person = new Person();
        person.setAge(1);
        person.setName("hello");
        return ResponseEntity.ok(person);
    }

}
