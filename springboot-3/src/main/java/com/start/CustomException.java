package com.start;

/**
 * ClassName: CustomException
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/8
 */
public class CustomException extends RuntimeException {
    public CustomException(String msg) {
        super(msg);
    }
}
