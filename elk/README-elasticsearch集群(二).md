#Elasticsearch集群搭建
##准备阶段
 1.  机器A(10.25.228.201) 机器B(10.165.111.12) 
 2.  elasticsearch-5.0.0.zip
##安装
解压 elasticsearch-5.0.0.zip
##配置
部署两个节点
###A节点
cluster.name: elasticsearch-app
http.port: 9200
node.name: node-A
network.host: 10.25.228.201
###B节点
cluster.name:elasticsearch-app
http.port:9200
node.name: node-B
network.host: 10.165.111.12
###上述是同一个网段下的配置，同一网段下elasticsearch是自动发现的，仔细看下我的是不同网段所以在上面的基础上面增加一些配置
###遇到问题
NotSerializableExceptionWrapper[no_route_to_host_exception: 没有到主机的路由]
关闭centos防火墙

方法2
iptables -F